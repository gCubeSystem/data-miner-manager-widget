This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "data-miner-manager-widget"


## [v1.6.0] 

- Updated to maven-portal-bom-4.0.0


## [v1.5.0] - 2019-10-01

- Added service info [ticket #12594]
- Added support to show log information [ticket #11711]
- Added support to show files html, json, pdf, txt [ticket #17106]
- Updated information show to the user when a computation is submitted [ticket #17030]
- Added Item Id support [ticket #16503]


## [v1.4.0] - 2019-04-01

- Added location and zoom support [ticket #11708]
- Added coordinates EPSG:4326 and EPSG:3857 support [ticket #11710]
	
	
## [v1.3.1] - 2018-10-01

- Updated to support Metadata by StorageHub[ticket #11879]
 

## [v1.3.0] - 2018-10-01
- Updated to support download by StorageHub[ticket #11879]
	
		
## [v1.2.0] - 2018-07-01
- Updated to support StorageHub[ticket #11879]
- Added support to netcdf files

 
## [v1.1.0] - 2017-06-12
 - Support Java 8 compatibility [ticket #8471]

            
## [v1.0.0] - 2017-03-31

 - First release
 
 
 
